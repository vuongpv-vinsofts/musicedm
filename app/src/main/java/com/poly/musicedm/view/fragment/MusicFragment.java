package com.poly.musicedm.view.fragment;

import android.view.View;

import com.poly.musicedm.R;
import com.poly.musicedm.view.base.BaseFragment;

/**
 * Created by Vuong_IT on 28/10/2017.
 */

public class MusicFragment extends BaseFragment {
    @Override
    protected void initData() {

    }

    @Override
    protected void initView(View v) {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_music;
    }
}
